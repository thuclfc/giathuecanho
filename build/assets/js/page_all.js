/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  $('.navbar-toggler').click(function () {
    $('header .navbar').toggleClass('active');
    $('.modal-navbar').addClass('is-open');
  });
  $('.navbar-collapse .close,.modal-navbar').click(function () {
    $('.navbar').removeClass('active');
    $('.modal-navbar').removeClass('is-open');
  });
  $('.description__title').on('click', function () {
    $(this).parent().toggleClass('show');
  });
  $(window).on("load", function (e) {
    $(".navbar-nav .sub-menu,.navbar-nav .sub-menu1").parent("li").append("<span class='show-menu'></span>");
    $('.show-menu').on('click', function () {
      $(this).parent().toggleClass('show');
    });
  }); // active navbar of page current

  var urlcurrent = window.location.href;
  $(".navbar-nav > li a[href$='" + urlcurrent + "']").addClass('active');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 199) {
      $('.navbar').addClass('fixed');
    } else {
      $('.navbar').removeClass('fixed');
    }
  });
  $('.backtop').on('click', function () {
    $('html,body').animate({
      scrollTop: 0
    }, 1000);
  });
  $('.btn_advanced').on('click', function () {
    $('.search .advanced__expand').toggleClass('show');
  });
  $('.filter-expand button').on('click', function () {
    $('.filter-expand .expand').toggleClass('show');
  });
  $('.project__filter .filter').on('click', function () {
    $('.project__filter .filter').removeClass('active');
    $(this).addClass('active');
  });
});